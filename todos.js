var express = require('express');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');

var User = require('./models/User');

// express.Router is a class to create modular, mountable route handlers
var router = express.Router();

function getCondition(status, last_usage) {
	var status1 = {condition: new Date().getTime() - 4 * 24 * 60 * 60 * 1000 ,
		           emailMsg: 'Your last usage was more than 4 days ago, we change your status to NOT RESPONSIVE and will send you message every 3 days'};
    var status2 = {condition: new Date().getTime() - 2 * 24 * 60 * 60 * 1000 ,
		           emailMsg: 'Your last usage was more than 2 days ago, we change your status to NOT ACTIVE and will send you message every 1 month'};
    var status3 = {condition: new Date().getTime() - 2 * 24 * 60 * 60 * 1000 ,
		           emailMsg: 'Your last usage was less than 2 days ago, we change your status to ACTIVE and will send you message everyday'};


	var lastUsageDate = new Date(last_usage).getTime();
	var result = 'Active';

	if (status == 'Active') {
		if (status1.condition > lastUsageDate) {
			result = 'Not Responsive';
			sendEmailCustom(status1.emailMsg);
		}
	} 
	else if(status == 'Not Responsive') {
		if (status2.condition > lastUsageDate) {
			result = 'Not Active';
			sendEmailCustom(status2.emailMsg);
		}
		else if (status3.condition < lastUsageDate) {
			result = result;
			sendEmailCustom(status3.emailMsg);
		}
	}

	return result;
}

function sendEmailCustom(message) {
	let transporter = nodemailer.createTransport({
        host: 'mail.wellindo.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'melvintoni@wellindo.com',
            pass: 'B=AwzEtZQ%D2'
        },
        // Use tls only when execute nodemailer in local/not actual website so it won't treated as spam
        tls: {
        	rejectUnauthorized: false
        }
    });

    let mailOptions = {
        from: '"Melvin Wellindo 👻" <melvintoni@wellindo.com>',
        to: 'melvin.working@gmail.com', // list of receivers
        subject: 'Notification only',
        text: 'Hello user', // plain text body
        html: '<i>'+message+'</i>' // html body
    };

	    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) { return console.log('THE ERROR : ' + error) };
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
}

router.get('/', function(req, res){
	User.find({}, function(err, result){
		if (err) {
			console.log(err);
		}
		else {
			res.render('index', {
				title: 'User List',
				items: result
			});
		}
	});
});

router.get('/send/:id', function(req, res){
	var id = req.params.id;

  	User.findById({_id:id}, function(err, user) {
	    if (err) {
	      console.log('error, no entry found');
	    }
	    user.status = getCondition(user.status, user.last_active);
	    user.save();
  	});
  	
  	res.redirect('/');
});

module.exports = router;