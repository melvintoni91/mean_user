var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var app = express();

// configure app
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// use middleware
// use will register middleware to express app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'bower_components')));

// connect db
// mongoose.connect('mongodb://localhost:27017/university');
mongoose.connect('mongodb://MelvinToniGustave:Weibu2@ds149682.mlab.com:49682/mean-user');
var connection = mongoose.connection;
connection.on('error', console.error.bind(console, 'connection error:'));
connection.once('open', function(){
	console.log('MONGODB IS CONNECTED SUCCESSFULLY');
});

// define routes
app.use(require('./todos'));

// start server
var port = process.env.PORT || 3000;
app.listen(port, function(){
	console.log('Port ' + port + ' ready');
});