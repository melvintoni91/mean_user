var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  	name:  { type: String },
    last_active: { type: String },
    status: { type: String }
});

module.exports = mongoose.model('User', userSchema, 'user');